from rest_framework import serializers
from .models import Error

class ErrorsDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Error
        fields = "__all__"