from django.contrib import admin
from django.urls import path, include
from .views import *

app_name = 'error'
urlpatterns = [
    path('errors', getErr),
]