from django.shortcuts import render
from rest_framework import generics
from rest_framework.response import Response
from django.db.models import Q
from django.http import JsonResponse
from .models import Error
from .serializers import ErrorsDetailSerializer


def getErr(request):
    if request.method == 'GET':
        q = Error.objects.all()
        if(len(request.GET)!=0):
            if (request.GET.get("worker")):
                q = q.filter(worker_id=request.GET.get("worker"))
            if (request.GET.get("machine")):
                q = q.filter(machine_id=request.GET.get("machine"))
            if (request.GET.get("end_date")):
                q = q.filter(end_date=request.GET.get("end_date"))
            if (request.GET.get("code")):
                q = q.filter(code=request.GET.get("code"))
            if (request.GET.get("start_date")):
                q = q.filter(start_date=request.GET.get("start_date"))
        #print(q[0].__dict__)
        serializer = ErrorsDetailSerializer(q, many=True)
        return JsonResponse(serializer.data, safe=False)
