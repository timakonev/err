from django.db import models

class Error(models.Model):
    machine = models.ForeignKey('Machine', on_delete=models.PROTECT)
    worker = models.ForeignKey('Worker', on_delete=models.PROTECT)
    date = models.DateTimeField(auto_now_add=True)
    value = models.IntegerField()
    code = models.IntegerField()

class Machine(models.Model):
    uniqueNumber = models.IntegerField()
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

class Worker(models.Model):
    uniqueNumber = models.IntegerField()
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name
